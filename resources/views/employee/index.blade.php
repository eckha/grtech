@extends('adminlte::page')

@section('title', 'Employee Data')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4 class="m-0">Employee Data</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Employee</a></li>
                    <li class="breadcrumb-item active">Employee Data</li>
                </ol>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="/employee/create" title="" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Add Data</a>
                    </div>
                    <div class="card-body">
                        <table  class="table table-bordered table-hover" id="datashow">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Full Name</th>
                                    <th>Company</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal fade" id="detailCompany">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bg-info">
                                <h4 class="modal-title" id="companyName"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
      
                            <div class="modal-body text-center">
                                <img id="companyLogo" src="" alt="" class="img-fluid">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b id="companyEmail"></b> <a class="float-right"><i class="fas fa-envelope"></i></a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href="" id="companyWebsite" target="_blank"></a> <a class="float-right"><i class="fas fa-globe"></i></a>
                                    </li>
                                </ul>
                            </div>
      
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="editEmployee">
                    <div class="modal-dialog modal-lg ">
      
                        <form class="form-horizontal" enctype="multipart/form-data"  method="post" id="formEditEmployee">
                            @method('put')
                            @csrf
      
                            <div class="modal-content">
      
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title">Edit Data</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
      
                                <div class="modal-body">
                                
                                    <div class="form-group row">
                                        <label for="first_name" class="col-sm-2 col-form-label @error('first_name') text-danger @enderror">First Name</label>
                                        <div class="col-sm-10">
                                          <input type="text" name="first_name" class="form-control @error ('first_name') is-invalid @enderror" id="first_name" placeholder="First Name" value="{{ old('first_name') }}" autofocus>
                                          @error("first_name")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <label for="last_name" class="col-sm-2 col-form-label @error('last_name') text-danger @enderror">Last Name</label>
                                        <div class="col-sm-10">
                                          <input type="text" name="last_name" class="form-control @error ('last_name') is-invalid @enderror" id="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                                          @error("last_name")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        </div>
                                    </div>
                
                                    <div class="form-group row">
                                        <label for="company_id" class="col-sm-2 col-form-label @error('company_id') text-danger @enderror">Company</label>
                                        <div class="col-sm-10">
                                          <select class="form-control select2 @error ('company_id') is-invalid @enderror" style="width: 100%;" name="company_id" id="company_id">
                                          </select>
                                          @error("company_id")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        </div>
                                    </div>
                
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-2 col-form-label @error('email') text-danger @enderror">Email</label>
                                        <div class="col-sm-10">
                                          <input type="email" name="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Email" value="{{ old('email') }}">
                                          @error("email")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        </div>
                                    </div>
                
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-2 col-form-label @error('phone') text-danger @enderror">Phone</label>
                                        <div class="col-sm-10">
                                          <input type="text" name="phone" class="form-control @error ('phone') is-invalid @enderror" id="phone" placeholder="Phone" value="{{ old('phone') }}">
                                          @error("phone")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        </div>
                                    </div>

                                </div>
      
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
      
                            </div>
      
                        </form>
      
                    </div>
                </div>
            </div>

        </div>
    </div>

    @push('js')
        <form action="" id="delete-form" method="post">
            @method('delete')
            @csrf
        </form>
        <script>
            $('#datashow').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: "/employee",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', class: "text-center"},
                    {data: 'full_name', name: 'full_name'},
                    {data: 'company', name: 'company'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {
                        data: 'action', 
                        name: 'action', 
                        class: "text-center",
                        orderable: true, 
                        searchable: true
                    },
                ]
            });

            $(document).on("click", "#btnShowCompany", function() {
                let id = $(this).attr("data-company-id");
                $.ajax({
                    type: "GET",
                    url: "/company/show/"+id,
                    dataType: "json",
                    success: function(ress) {
                        if(ress.success == false) {
                            toastr.error(ress.msg);
                        } else {
                            let data = ress.company;
                            document.getElementById('companyName').innerHTML=data.name;
                            document.getElementById('companyWebsite').innerHTML=data.website;
                            document.getElementById('companyEmail').innerHTML=data.email;
                            $("#companyLogo").attr("src", "storage/"+data.logo);
                            $('#detailCompany').modal('show');
                        }
                    }
                });
            });

            $(document).on("click", "#btnEditEmployee", function() {
                let id = $(this).attr("data-id");
                $.ajax({
                    type: "GET",
                    url: "/employee/edit/"+id,
                    dataType: "json",
                    success: function(ress) {
                        if(ress.success == false) {
                            toastr.error(ress.msg);
                        } else {
                            let data = ress.employee;
                            let data2 = ress.company;
                            
                            let option_company = '<option value="">Company</optin>';
                            $('#editEmployee').modal('show');
                            $('#formEditEmployee').attr('action', 'employee/update/'+data.id);
                            $('#first_name').val(data.first_name);
                            $('#last_name').val(data.last_name);
                            $('#email').val(data.email);
                            $('#phone').val(data.phone);

                            for(let i=0; i<data2.length; i++) {
                                if (data2[i].id==data.company_id) {
                                    option_company +='<option value="'+data2[i].id+'" selected>'+data2[i].name+'</option>';
                                } else {
                                    option_company +='<option value="'+data2[i].id+'">'+data2[i].name+'</option>';
                                }
                            }

                            $("#company_id").html(option_company);
                        }
                        
                    }
                });
            });

            function notificationBeforeDelete(event, el) {
                event.preventDefault();
                Swal.fire({
                    title: 'Delete EMployee Data !',
                    text: "Are you sure to delete this employee data? ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                }).then((result) => {
                    if(result.value) {
                        $("#delete-form").attr('action', $(el).attr('href'));
                        $("#delete-form").submit();
                    }
                });
                
            }
        </script>
    @endpush
@stop
