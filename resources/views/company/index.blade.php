@extends('adminlte::page')

@section('title', 'Company Data')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4 class="m-0">Company Data</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Company</a></li>
                    <li class="breadcrumb-item active">Company Data</li>
                </ol>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a href="/company/create" title="" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> Add Data</a>
                    </div>
                    <div class="card-body">
                        <table  class="table table-bordered table-hover" id="datashow">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>Website</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal fade" id="editCompany">
                    <div class="modal-dialog modal-lg ">
      
                        <form class="form-horizontal" enctype="multipart/form-data"  method="post" id="formEditCompany">
                            @method('put')
                            @csrf
      
                            <div class="modal-content">
      
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title">Edit Data</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
      
                                <div class="modal-body">
                                
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-2 col-form-label @error('name') text-danger @enderror">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control @error ('name') is-invalid @enderror" id="name" placeholder="Name" value="{{ old('name') }}" autofocus>
                                            @error("name")
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <label for="email" class="col-sm-2 col-form-label @error('email') text-danger @enderror">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="Email" value="">
                                            @error("email")
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <label for="website" class="col-sm-2 col-form-label @error('website') text-danger @enderror">Website</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="website" class="form-control @error ('website') is-invalid @enderror" id="website" placeholder="Website" value="">
                                            @error("website")
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <label for="customFile" class="col-sm-2 col-form-label @error('logo') text-danger @enderror">Logo</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input @error ('logo') is-invalid @enderror" id="customFile" name="logo">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                            <p>Leave the logo blank if you don't want to change</p>
                                            @error("logo")
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
      
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
      
                            </div>
      
                        </form>
      
                    </div>
                </div>
            </div>

        </div>
    </div>

    @push('js')
        <form action="" id="delete-form" method="post">
            @method('delete')
            @csrf
        </form>
        <script>
            $('#datashow').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: "/company",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'logo', name: 'logo'},
                    {data: 'website', name: 'website'},
                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: true, 
                        searchable: true
                    },
                ]
            });

            $(document).on("click", "#btnEditCompany", function() {
                debugger;
                let id = $(this).attr("data-id");
                $.ajax({
                    type: "GET",
                    url: "/company/edit/"+id,
                    dataType: "json",
                    success: function(ress) {
                        if(ress.success == false) {
                            toastr.error(ress.msg);
                        } else {
                            let data = ress.company;
                            $('#editCompany').modal('show');
                            $('#formEditCompany').attr('action', '/company/update/'+data.id);
                            $('#name').val(data.name);
                            $('#website').val(data.website);
                            $('#email').val(data.email);
                        }
                        
                    }
                });
            });

            function notificationBeforeDelete(event, el) {
                event.preventDefault();
                Swal.fire({
                    title: 'Delete Company Data !',
                    text: "Are you sure to delete this company data? ?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                }).then((result) => {
                    if(result.value) {
                        $("#delete-form").attr('action', $(el).attr('href'));
                        $("#delete-form").submit();
                    }
                });
                
            }
        </script>
    @endpush
@stop
