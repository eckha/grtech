<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data_company = Company::orderBy("name")->get();
            return Datatables::of($data_company)
                ->addIndexColumn()
                ->editColumn('website', function ($row) {
                        $link = '<a href="'.$row->website.'" target="blank">'.$row->website.'</a>';
                        return $link;
                })
                ->editColumn('logo', function ($row) {
                    if($row->logo) {
                        $url= asset('storage/'.$row->logo);
                        return '<img src="'.$url.'" border="0" width="100%" />';
                    } else {
                        return "";
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button id="btnEditCompany" data-id="'.$row->id.'" class="edit btn btn-success btn-sm">Edit</button> 
                                 <a href="/company/delete/'.$row->id.'" onclick="notificationBeforeDelete(event, this)" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->escapeColumns([])
                ->make(true);
        }

        return view('company.index');
    }

   
    public function create()
    {
        return view('company.create');
    }

    public function store(Request $request)
    {
        $validatedData  = $request->validate([
            'name'     => 'required|max:250|unique:companies,name',
            'email'    => 'required|email|max:250|unique:companies,email',
            "logo"     => "nullable|image|file|mimes:jpg,jpeg,png,gif,webp|max:10240",
            'website'  => 'nullable|max:250'
        ]);

        if($request->file('logo')) {
            $validatedData['logo'] = $request->file('logo')->store('company-logo');
        }

        Company::create($validatedData);

        return redirect('/company')->with("success_message", "Company has been added successfully");
    }


    public function show($id)
    {
        $company = Company::find($id);

        if (!$company)
        {
            return response()->json([
                'success' => false,
                'msg' => "Company not found."
            ]);
        }

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'company' => $company
        ]);
    }

    public function edit($id)
    {
        $company = Company::find($id);

        if (!$company)
        {
            return response()->json([
                'success' => false,
                'msg' => "Failed, Company not found."
            ]);
        }

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'company' => $company
        ]);
    }

    
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $rulses = [
            'name'     => 'required|max:250|unique:companies,name,' . $id,
            'email'    => 'required|email|max:250|unique:companies,email,' .$id,
            "logo"     => "nullable|image|file|mimes:jpg,jpeg,png,gif,webp|max:10240",
            'website'  => 'nullable|max:250'
        ];

        $validatedData = $request->validate($rulses);

        if($request->file('logo')) {
            if($company->logo) {
                Storage::delete($company->logo);
            }
            $validatedData['logo'] = $request->file('logo')->store('company-logo');
        }

        Company::where('id', $id)->update($validatedData);
        return redirect('/company')->with("success_message", "Company has been updated successfully");
    }

    
    public function destroy($id)
    {
        $company = Company::find($id);

        if($company->logo) {
            Storage::delete($company->logo);
        }

        $company->delete();
        return redirect('/company')->with("success_message", "Company has been deleted successfully");
    }
}
