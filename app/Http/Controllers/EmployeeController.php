<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Employee;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use App\Notifications\sendNotification;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{
   
    public function index(Request $request)
    {
        if ($request->ajax()) {

	        $data_employee = Employee::with('company')->get();

	        return Datatables::of($data_employee)
	            ->addIndexColumn()
	            ->addColumn('company', function ($row) {
                        $link = '<span class="btn-link" id="btnShowCompany" data-company-id="'.$row->company_id.'" style="cursor:pointer;">'.$row->company->name.'</span>';
                        return $link;
                })
	            ->addColumn('full_name', function ($row) {
	                    $full_name = $row->full_name;
	                    return $full_name;
	            })
	            ->addColumn('action', function($row){
	                $actionBtn = '<button id="btnEditEmployee" data-id="'.$row->id.'" class="edit btn btn-success btn-sm">Edit</button> 
                                <a href="/employee/delete/'.$row->id.'" onclick="notificationBeforeDelete(event, this)" class="delete btn btn-danger btn-sm">Delete</a>';
	                return $actionBtn;
	            })
	            ->rawColumns(['action'])
	            ->escapeColumns([])
	            ->make(true);
	    }

        return view('employee.index');
    }

    
    public function create()
    {
        $data_company = Company::orderBy("name")->get();

        return view("employee.create", [
            "data_company"      => $data_company,
        ]);
    }


    public function store(Request $request)
    {
        $validatedData  = $request->validate([
            'first_name'    => 'required|max:25',
            'last_name'     => 'required|max:25',
            'company_id'    => 'required',
            'email'         => 'nullable|email|max:250|unique:employees,email',
            'phone'         => 'nullable|unique:employees,phone'
        ]);

        $employee = Employee::create($validatedData);

        return redirect('/employee/sendNotification/'.$employee->id);

        // return redirect('/employee')->with("success_message", "Employee has been added successfully");
    }

    public function sendNotification($id) 
    {
        $employee = Employee::find($id);
        $company_id = $employee->company_id;
        $company = Company::find($company_id);

        $details = [
            'greeting' => 'Hi '.$company->name,
            'body' => $employee->full_name.' has been added as an employee to your company',
            'actiontext' => 'Click here to see details',
            'actionturl' => '/',
            'lastline' => 'Thank You'
        ];

        Notification::send($company, new sendNotification($details));

        return redirect('/employee')->with("success_message", "Employee has been added successfully");
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $employee = Employee::find($id);
        $data_company = Company::orderBy("name")->get();

        if (!$employee)
        {
            return response()->json([
                'success' => false,
                'msg' => "Employee not found."
            ]);
        }

        return response()->json([
            'success' => true,
            'msg' => "OK",
            'employee' => $employee,
            'company' => $data_company
        ]);
    }


    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'first_name'    => 'required|max:25',
            'last_name'     => 'required|max:25',
            'company_id'    => 'required',
            'email'         => 'nullable|email|max:250|unique:employees,email,' .$id,
            'phone'         => 'nullable|unique:employees,phone,' .$id

        ]);

        Employee::where('id', $id)->update($validatedData);
        return redirect('/employee')->with("success_message", "Employee has been updated successfully");
    }

    
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();

        return redirect('/employee')->with("success_message", "Employee has been deleted successfully");
    }
}
