<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(["middleware" => "auth"], function () {

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(["middleware" => "admin"], function () {

        Route::get('/company', [App\Http\Controllers\CompanyController::class, 'index']);
        Route::get('/company/create', [App\Http\Controllers\CompanyController::class, 'create']);
        Route::post('/company/store', [App\Http\Controllers\CompanyController::class, 'store']);
        Route::any("/company/show/{id}", [App\Http\Controllers\CompanyController::class, 'show']);
        Route::get('/company/edit/{id}', [App\Http\Controllers\CompanyController::class, 'edit']);
        Route::put('/company/update/{id}', [App\Http\Controllers\CompanyController::class, 'update']);
        Route::delete('/company/delete/{id}', [App\Http\Controllers\CompanyController::class, 'destroy']);

        Route::get('/employee', [App\Http\Controllers\EmployeeController::class, 'index']);
        Route::get('/employee/create', [App\Http\Controllers\EmployeeController::class, 'create']);
        Route::post('/employee/store', [App\Http\Controllers\EmployeeController::class, 'store']);
        Route::get('/employee/edit/{id}', [App\Http\Controllers\EmployeeController::class, 'edit']);
        Route::put('/employee/update/{id}', [App\Http\Controllers\EmployeeController::class, 'update']);
        Route::delete('/employee/delete/{id}', [App\Http\Controllers\EmployeeController::class, 'destroy']);
        Route::get('employee/sendNotification/{id}', [App\Http\Controllers\EmployeeController::class, 'sendNotification'])->name("sendNotification");

    });
});


